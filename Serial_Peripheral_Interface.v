/**
* Humberto Alvarez | 2020
*
* Serial Peripheral Interface (SPI)
*
*/


`timescale  1ns/1ns

module master_device(DATA_ADDR, SS_ADDR, SCLK, MOSI, MISO, SS);
// I/O PORTS
input        MISO;
output  reg  MOSI, SCLK;
input   [1:0] SS_ADDR;
output  reg [3:0] SS;
input   [7:0] DATA_ADDR;

reg [7:0] DATA;

integer i = 0;
integer j = 0;

always@(DATA_ADDR, SS_ADDR)
begin
  for (j = 0; j < 4; j = j+1) begin
    SS[j] = 0;
  end
  for(i = 0; i< 8;i = i+1) begin
    SCLK = 0; #10;
    MOSI = DATA_ADDR[i];
    SCLK = 1; #10;
  end
  for(i = 0; i< 8;i = i+1) begin
    SCLK = 0; #10;
    SCLK = 1; #10;
    DATA[i] = MISO;
  end
end

endmodule

module slave_device(SCLK, MOSI, MISO, SS);
// I/O PORTS
input       MOSI, SCLK, SS;
output  reg MISO;

// INTERNAL DATA REGISTERS
reg [7:0] reg0 = 8'h41; // Address = 8'h1A
reg [7:0] reg1 = 8'hDC; // Address = 8'h1B
reg [7:0] reg2 = 8'h3B; // Address = 8'h1C
reg [7:0] reg3 = 8'h4E; // Address = 8'h1D
reg [7:0] reg4 = 8'h8C; // Address = 8'h2A
reg [7:0] reg5 = 8'hB5; // Address = 8'h2B
reg [7:0] reg6 = 8'h05; // Address = 8'h2C
reg [7:0] reg7 = 8'hE5; // Address = 8'h2D

reg [7:0] ADDR_Comp = 8'h00;
reg [3:0] counter = 0; 

always@(posedge SCLK) begin
  if (SS == 0) begin
    if (counter < 8) begin
      ADDR_Comp[counter] = MOSI;
    end
    else begin
      if (ADDR_Comp == 8'h1A) 
        MISO = reg0[counter - 8];
      else if(ADDR_Comp == 8'h1B)
        MISO = reg1[counter - 8];
      else if(ADDR_Comp == 8'h1C)
        MISO = reg2[counter - 8];
      else if(ADDR_Comp == 8'h1D)
        MISO = reg3[counter - 8];
      else if(ADDR_Comp == 8'h2A)
        MISO = reg4[counter - 8];
      else if(ADDR_Comp == 8'h2B)
        MISO = reg5[counter - 8];
      else if(ADDR_Comp == 8'h2C)
        MISO = reg6[counter - 8];
      else if(ADDR_Comp == 8'h2D)
        MISO = reg7[counter - 8];
    end
    counter = counter + 1;
  end
end


endmodule
